import React, { useEffect, useState } from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import "../../global/form.css";
import api from "../../services/api";

const AdminForm = () => {
	const [loading, setLoading] = useState(false);
	const [form, setForm] = useState({ login: "", password: "" });

	const history = useHistory();
	const match = useRouteMatch();
	const _id = match.params?.id;

	const getAdmin = async (_id) => {
		if (_id) {
			setLoading(true);

			const res = await api.get(`/admin/${_id}`);
			setForm(res.data);
			setLoading(false);
		}
	};

	useEffect(() => {
		getAdmin(_id);
	}, [_id]);

	const handleChange = (e) => {
		setForm({
			...form,
			[e.target.name]: e.target.value,
		});
	};

	const voltar = () => {
		history.replace("/admin");
	};

	const handleSalvar = async (e) => {
		e.preventDefault();
		setLoading(true);

		try {
			const { login, password } = form;

			if (_id) {
				await api.put(`/admin/${_id}`, { login, password });
			} else {
				await api.post("/admin/", { login, password });
			}

			setLoading(false);
			voltar();
		} catch (error) {
			setLoading(false);
			console.log(error);
		}
	};

	return (
		<div className="contentForm">
			{loading && <div>Aguarde...</div>}

			{!loading && (
				<form onSubmit={handleSalvar}>
					<div>
						<span>Login</span>
						<input value={form.login} name="login" onChange={(e) => handleChange(e)} />
					</div>
					<div>
						<span>Password</span>
						<input value={form.password} name="password" onChange={(e) => handleChange(e)} type="password" />
					</div>
					<button type="submit" disabled={loading}>
						Salvar
					</button>
					<button className="btnVoltar" onClick={voltar}>
						Voltar
					</button>
				</form>
			)}
		</div>
	);
};

export default AdminForm;
