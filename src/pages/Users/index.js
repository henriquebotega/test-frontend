import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import io from "socket.io-client";
import "../../global/page.css";
import api, { ioURL } from "../../services/api";

const Users = () => {
	const [users, setUsers] = useState([]);
	const [loading, setLoading] = useState(false);
	const history = useHistory();

	useEffect(() => {
		const socket = io(ioURL);

		socket.on("newUser", (payload) => {
			console.log("Users -> new", users);
			// setUsers([...users, payload]);
		});

		socket.on("editUser", (payload) => {
			console.log("Users -> edit", users);
			// const items = users.filter((obj) => obj._id !== payload._id);
			// setUsers(items);
		});

		socket.on("delUser", (payload) => {
			console.log("Users -> del", users);
			// const items = users.filter((obj) => obj._id !== payload._id);
			// setUsers(items);
		});
	}, []);

	const getUsers = async () => {
		setLoading(true);

		const res = await api.get("/users");
		setUsers(res.data);
		setLoading(false);
	};

	useEffect(() => {
		getUsers();
	}, []);

	const incluir = async () => {
		history.replace(`/users/form`);
	};

	const editar = async ({ _id }) => {
		history.replace(`/users/form/${_id}`);
	};

	const excluir = async ({ _id }) => {
		await api.delete(`/users/${_id}`);
		const currentList = users.filter((obj) => obj._id !== _id);
		setUsers(currentList);
	};

	return (
		<div className="contentPage">
			{loading && <div>Aguarde...</div>}

			{!loading && (
				<div>
					<button className="btnIncluir" onClick={() => incluir()}>
						Novo
					</button>

					<ul>
						{users.map((item, i) => (
							<li key={i}>
								{item.name} - {item.email}
								<button className="btnEditar" onClick={() => editar(item)}>
									Editar
								</button>
								<button className="btnExcluir" onClick={() => excluir(item)}>
									Excluir
								</button>
							</li>
						))}
					</ul>
				</div>
			)}
		</div>
	);
};

export default Users;
