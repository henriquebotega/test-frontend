import React from "react";

const Home = ({ isLoggedIn }) => {
	return (
		<div>
			<h1>Home</h1>
			{!isLoggedIn && <span>Voce nao esta conectado</span>}
			{isLoggedIn && <span>Seja bem vindo</span>}
		</div>
	);
};

export default Home;
