import React, { useEffect, useState } from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import "../../global/form.css";
import api from "../../services/api";

const UserForm = () => {
	const [loading, setLoading] = useState(false);
	const [form, setForm] = useState({ name: "", email: "" });

	const history = useHistory();
	const match = useRouteMatch();
	const _id = match.params.id;

	const getUser = async (_id) => {
		if (_id) {
			setLoading(true);

			const res = await api.get(`/users/${_id}`);
			setForm(res.data);
			setLoading(false);
		}
	};

	useEffect(() => {
		getUser(_id);
	}, [_id]);

	const handleChange = (e) => {
		setForm({
			...form,
			[e.target.name]: e.target.value,
		});
	};

	const voltar = () => {
		history.replace("/users");
	};

	const handleSalvar = async (e) => {
		e.preventDefault();

		try {
			const { name, email } = form;

			if (_id) {
				await api.put(`/users/${_id}`, { name, email });
			} else {
				await api.post("/users/", { name, email });
			}

			voltar();
		} catch (error) {
			console.log(error);
		}
	};

	return (
		<div className="contentForm">
			{loading && <div>Aguarde...</div>}

			{!loading && (
				<form onSubmit={handleSalvar}>
					<div>
						<span>Login</span>
						<input value={form.name} name="name" onChange={(e) => handleChange(e)} />
					</div>
					<div>
						<span>E-mail</span>
						<input value={form.email} name="email" onChange={(e) => handleChange(e)} type="email" />
					</div>
					<button type="submit" disabled={loading}>
						Salvar
					</button>
					<button className="btnVoltar" onClick={voltar}>
						Voltar
					</button>
				</form>
			)}
		</div>
	);
};

export default UserForm;
