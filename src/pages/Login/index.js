import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import api from "../../services/api";
import "./styles.css";

const Login = ({ checkLocalStorage }) => {
	const [form, setForm] = useState({ login: "", password: "" });
	const [loading, setLoading] = useState(false);

	let history = useHistory();

	useEffect(() => {
		if (localStorage.getItem("isLoggedIn")) {
			history.replace("/");
		}
	}, [history]);

	const handleValidar = async (e) => {
		e.preventDefault();
		const { login, password } = form;

		if (login && password) {
			setLoading(true);

			try {
				const res = await api.post("/login", { login, password });

				if (res.data.auth) {
					setLoading(false);
					checkLocalStorage();
					history.replace("/");
				}
			} catch (error) {
				setLoading(false);
				console.log(error);
			}
		}
	};

	const handleChange = (e) => {
		setForm({
			...form,
			[e.target.name]: e.target.value,
		});
	};

	return (
		<div className="contentLogin">
			{loading && <div>Aguarde...</div>}

			{!loading && (
				<form onSubmit={handleValidar}>
					<div>
						<span>Login</span>
						<input value={form.login} name="login" onChange={(e) => handleChange(e)} />
					</div>
					<div>
						<span>Password</span>
						<input value={form.password} name="password" onChange={(e) => handleChange(e)} type="password" />
					</div>
					<button type="submit" disabled={loading}>
						Validar
					</button>
				</form>
			)}
		</div>
	);
};

export default Login;
