import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import "../../global/page.css";
import api from "../../services/api";

const Admin = () => {
	const [admins, setAdmins] = useState([]);
	const [loading, setLoading] = useState(false);
	const history = useHistory();

	const getAdmin = async () => {
		setLoading(true);

		const res = await api.get("/admin");
		setAdmins(res.data);
		setLoading(false);
	};

	useEffect(() => {
		getAdmin();
	}, []);

	const incluir = async () => {
		history.replace(`/admin/form`);
	};

	const editar = async ({ _id }) => {
		history.replace(`/admin/form/${_id}`);
	};

	const excluir = async ({ _id }) => {
		await api.delete(`/admin/${_id}`);
		const currentList = admins.filter((obj) => obj._id !== _id);
		setAdmins(currentList);
	};

	return (
		<div className="contentPage">
			{loading && <span>Aguarde...</span>}

			{!loading && (
				<div>
					<button className="btnIncluir" onClick={() => incluir()}>
						Novo
					</button>

					<ul data-testid="listAdmin">
						{admins.map((item, i) => (
							<li key={i}>
								{item.login} - {item.password}
								<button className="btnEditar" onClick={() => editar(item)}>
									Editar
								</button>
								<button className="btnExcluir" onClick={() => excluir(item)}>
									Excluir
								</button>
							</li>
						))}
					</ul>
				</div>
			)}
		</div>
	);
};

export default Admin;
