import { act, cleanup, render } from "@testing-library/react";
import MockAdapter from "axios-mock-adapter";
import React from "react";
import Header from "../components/Header";
import Admin from "../pages/Admin";
import AdminForm from "../pages/AdminForm";
import Home from "../pages/Home";
import api from "../services/api";

const apiMock = new MockAdapter(api);

const wait = (amount = 0) => {
	return new Promise((resolve) => setTimeout(resolve, amount));
};

const actWait = async (amount = 0) => {
	await act(async () => {
		await wait(amount);
	});
};

jest.mock("react-router-dom", () => ({
	useHistory: () => ({
		push: jest.fn(),
		replace: jest.fn(),
	}),
	useRouteMatch: () => ({
		match: {
			params: {
				id: 1,
			},
		},
	}),
}));

beforeEach(cleanup);

describe("Testing our App", () => {
	test("Verifying Header", () => {
		const { getByText } = render(<Header />);
		expect(getByText(/Home/i)).toBeInTheDocument();
		expect(getByText(/Admin/i)).toBeInTheDocument();
		expect(getByText(/Login/i)).toBeInTheDocument();
	});

	test("Verifying Home", () => {
		const { getByText } = render(<Home />);
		expect(getByText(/Voce nao esta conectado/i)).toBeInTheDocument();
	});

	test("Verifying Admin", async () => {
		const { getByText, getByTestId } = render(<Admin />);

		apiMock.onGet("/admin").reply(200, [
			{ _id: "5e8f6c98b532c82d1b8c5ecc", login: "teste", password: "123" },
			{ _id: "5e9521e8e5d9d40004d4cb34", login: "henrique", password: "hen" },
		]);

		expect(getByText("Aguarde...")).toBeInTheDocument();
		await actWait(5);

		expect(getByTestId("listAdmin")).toContainElement(getByText("teste - 123"));
	});

	test("Verifying AdminForm", async () => {
		const { getByText } = render(<AdminForm />);

		expect(getByText(/Login/i)).toBeInTheDocument();
		expect(getByText(/Password/i)).toBeInTheDocument();
		expect(getByText(/Salvar/i)).toBeInTheDocument();
		expect(getByText(/Voltar/i)).toBeInTheDocument();
	});
});
