import { mount, render, shallow } from "enzyme";
import React from "react";
import { MemoryRouter } from "react-router-dom";
import App from "../App";
import Header from "../components/Header";
import Login from "../pages/Login";

jest.mock("react-router-dom", () => ({
	useHistory: () => ({
		push: jest.fn(),
		replace: jest.fn(),
	}),
	useRouteMatch: () => ({
		match: {
			params: {
				id: 1,
			},
		},
	}),
}));

describe("MyComponent", () => {
	test("should shallow", () => {
		const wrapper = mount(
			<MemoryRouter initialEntries={["/"]}>
				<App />
			</MemoryRouter>
		);

		expect(wrapper.find(Header).length).toBe(1);
		expect(wrapper.find(Header).get(0).props.isLoggedIn).toBe(false);
	});

	test("should shallow", () => {
		const wrapper = shallow(<Login />);

		const inputLogin = wrapper.find("input").get(0);
		// inputLogin.simulate("focus");
		// inputLogin.simulate("change", { target: { value: "test" } });

		const inputPassword = wrapper.find("input").get(1);
		// inputPassword.simulate("focus");
		// inputPassword.simulate("change", { target: { value: "123456" } });

		wrapper.find("button").simulate("click");
		expect(wrapper).toMatchSnapshot();
	});

	test("should render", () => {
		const wrapper = render(<Login />);
		expect(wrapper.text()).toBe("LoginPasswordValidar");
	});
});
