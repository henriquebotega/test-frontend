import React from "react";
import ReactDOM, { unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Header from "../components/Header";

jest.mock("react-router-dom", () => ({
	useHistory: () => ({
		push: jest.fn(),
		replace: jest.fn(),
	}),
}));

let container = null;

beforeEach(() => {
	container = document.createElement("div");
	document.body.append(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

const logout = jest.fn();
let isLoggedIn;

describe("Check Header functionallyties", () => {
	it("Render Header", () => {
		isLoggedIn = false;

		act(() => {
			ReactDOM.render(<Header isLoggedIn={isLoggedIn} logout={logout} />, container);
		});

		const btnLogin = container.querySelector("[data-testid=btnLogin]");
		expect(btnLogin.textContent).toBe("Login");

		const btnUsers = container.querySelector("[data-testid=btnUsers]");
		expect(btnUsers).toBe(null);

		const btnLogout = container.querySelector("[data-testid=btnLogout]");
		expect(btnLogout).toBe(null);
	});

	it("Render Header", () => {
		isLoggedIn = true;

		act(() => {
			ReactDOM.render(<Header isLoggedIn={isLoggedIn} logout={logout} />, container);
		});

		const btnLogin = container.querySelector("[data-testid=btnLogin]");
		expect(btnLogin).toBe(null);

		const btnUsers = container.querySelector("[data-testid=btnUsers]");
		expect(btnUsers.textContent).toBe("Users");

		const btnLogout = container.querySelector("[data-testid=btnLogout]");
		expect(btnLogout.textContent).toBe("Logout");
	});
});
