import React from "react";
import { useHistory } from "react-router-dom";
import "./styles.css";

const Header = ({ isLoggedIn, logout }) => {
	const history = useHistory();

	const goTo = (page) => {
		history.replace(page);
	};

	const sair = () => {
		logout();
		goTo("/");
	};

	return (
		<div className="contentHeader">
			<button onClick={() => goTo("/")}>Home</button>
			<button onClick={() => goTo("/admin")}>Admin</button>

			{!isLoggedIn && (
				<button data-testid="btnLogin" onClick={() => goTo("/login")}>
					Login
				</button>
			)}

			{isLoggedIn && (
				<>
					<button data-testid="btnUsers" onClick={() => goTo("/users")}>
						Users
					</button>
					<button data-testid="btnLogout" onClick={() => sair()}>
						Logout
					</button>
				</>
			)}
		</div>
	);
};

export default Header;
