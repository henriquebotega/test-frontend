import axios from "axios";

export const ioURL = "https://test-backend-token.herokuapp.com";

const api = axios.create({
	baseURL: ioURL + "/api",
});

api.interceptors.request.use(
	function (config) {
		config.headers["x-access-token"] = localStorage.getItem("isLoggedIn");
		return config;
	},
	function (error) {
		return Promise.reject(error);
	}
);

api.interceptors.response.use(
	function (response) {
		if (response.config.url.indexOf("/login") > -1) {
			localStorage.setItem("isLoggedIn", response.data.token);
		}

		return response;
	},
	function (error) {
		return Promise.reject(error);
	}
);

export default api;
