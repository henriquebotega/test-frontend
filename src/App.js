import React, { useEffect, useState } from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Header from "./components/Header";
import Admin from "./pages/Admin";
import AdminForm from "./pages/AdminForm";
import Home from "./pages/Home";
import Login from "./pages/Login";
import UserForm from "./pages/UserForm";
import Users from "./pages/Users";

const isAuthenticated = () => {
	return !!localStorage.getItem("isLoggedIn");
};

const PrivateRoute = ({ children, ...rest }) => {
	return <Route {...rest} render={() => (isAuthenticated() ? children : <Redirect to="/login" />)} />;
};

const App = () => {
	const [isLoggedIn, setIsLoggedIn] = useState(false);

	const checkLocalStorage = () => {
		setIsLoggedIn(!!localStorage.getItem("isLoggedIn"));
	};

	useEffect(() => {
		checkLocalStorage();
	}, []);

	const logout = () => {
		localStorage.removeItem("isLoggedIn");
		checkLocalStorage();
	};

	return (
		<>
			<BrowserRouter>
				<Header isLoggedIn={isLoggedIn} logout={logout} />

				<Switch>
					<Route path="/" exact render={() => <Home isLoggedIn={isLoggedIn} />} />
					<Route path="/login" render={() => <Login checkLocalStorage={checkLocalStorage} />} />
					<Route path="/admin" exact component={Admin} />
					<Route path="/admin/form/:id?" component={AdminForm} />

					<PrivateRoute path="/users" exact>
						<Users />
					</PrivateRoute>

					<PrivateRoute path="/users/form/:id?">
						<UserForm />
					</PrivateRoute>
				</Switch>
			</BrowserRouter>
		</>
	);
};

export default App;
